﻿using System;
using System.Collections.Generic;
using DataLayer;
using DataLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTesting
{
    [TestClass]
    public class ReservationRepoTest
    {
        private ReservationRepository reservationRepository = new ReservationRepository();
        private GuestRepository guestRepository = new GuestRepository();
        private RoomRepository roomRepository = new RoomRepository();
        private Reservation reservation;
        private Guest guest;
        private Room room;

        [TestInitialize]
        public void init()
        {
            guest = new Guest();
            guest.Id = "TEST123ID";
            guest.Firstname = "TESTFIRSTNAME";
            guest.Lastname = "TESTLASTNAME";
            guest.Address = "TESTADDRESS";
            guest.BirthDate = DateTime.Now;
            guest.Phone = "00000000";
            guest.Email = "testmail";

            guestRepository.AddNewGuest(guest);

            room = new Room();
            room.Id = 999;
            room.TypeID = 1;
            room.Category = 5;
            room.Floor = 3;
            room.Price = 1000;
            room.Note = " ";

            roomRepository.AddNewRooms(room);

            reservation = new Reservation();
            reservation.GuestId = guest.Id;
            reservation.RoomId = room.Id;
            reservation.Payment = 4224;
            reservation.BeginDate = DateTime.Now;
            reservation.EndDate = DateTime.Now;

        }
        [TestMethod]
        public void TestAddingNewReservation()
        {
            reservationRepository.AddNewReservation(reservation);

            reservation = reservationRepository.GetReservationByGuest(guest);

            if(reservation == null)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TestGetReservationByGuest()
        {
            reservationRepository.AddNewReservation(reservation);

            reservation = reservationRepository.GetReservationByGuest(guest);

            if (reservation.GuestId != guest.Id)
            {
                Assert.Fail();
            }

        }

        [TestMethod]
        public void TestDeleteReservation()
        {
            reservationRepository.AddNewReservation(reservation);

            reservation = reservationRepository.GetReservationByGuest(guest);
            if (reservation == null)
                Assert.Fail();
            reservationRepository.DeleteReservation(reservation.Id);

            foreach (Reservation x in reservationRepository.GetAllReservations())
            {
                if (x.Id == reservation.Id)
                    Assert.Fail();
            }
        }

        [TestCleanup]
        public void cleanup()
        {
            reservationRepository.DeleteReservation(reservation.Id);
            guestRepository.DeleteGuest(guest.Id);
            roomRepository.DeleteRooms(room.Id);

        }
    }
}
