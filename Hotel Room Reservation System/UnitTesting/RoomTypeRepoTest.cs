﻿using System;
using System.Collections.Generic;
using DataLayer;
using DataLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTesting
{
    [TestClass]
    public class RoomTypeRepoTest
    {
        private RoomTypeRepository roomTypeRepository = new RoomTypeRepository();
        private RoomType roomType;

        [TestInitialize]
        public void init()
        {
            roomType = new RoomType();
            roomType.Type = "TEST TYPE";
        }
        [TestMethod]
        public void TestAddingNewRoomType()
        {
            roomTypeRepository.AddNewRoomType(roomType);

            List<RoomType> roomTypes = roomTypeRepository.GetAllRoomTypes();

            bool notIn = true;
            foreach (RoomType r in roomTypes)
            {
                if (roomType.Type == r.Type)
                {
                    notIn = false;
                    break;
                }
            }
            if (notIn)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TestGettingTypeByName()
        {
            roomTypeRepository.AddNewRoomType(roomType);

            List<RoomType> roomTypes = roomTypeRepository.GetAllRoomTypes();

            if (roomTypeRepository.GetRoomTypeByName(roomType.Type).Type != "TEST TYPE")
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TestDeleteRoomType()
        {
            roomTypeRepository.AddNewRoomType(roomType);
            roomTypeRepository.RemoveRoomType(roomType);

            foreach (RoomType x in roomTypeRepository.GetAllRoomTypes())
            {
                if (x.Id == roomType.Id)
                    Assert.Fail();
            }
        }


        [TestCleanup]
        public void cleanup()
        {
            roomTypeRepository.RemoveRoomType(roomType);
        }
    }
}
