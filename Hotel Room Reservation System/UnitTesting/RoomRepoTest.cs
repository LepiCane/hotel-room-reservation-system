﻿using System;
using DataLayer;
using DataLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTesting
{
    [TestClass]
    public class RoomRepoTest
    {
        private RoomRepository roomRepository = new RoomRepository();
        private Room room;

        [TestInitialize]
        public void init()
        {
            room = new Room();
            room.Id = 1999;
            room.TypeID = 2;
            room.Category = 5;
            room.Floor = 5;
            room.Price = 100;
            room.Note = "Note";
        }

        [TestMethod]
        public void TestAddNewRooms()
        {
            roomRepository.AddNewRooms(room);

            bool added = false;
            foreach (Room r in roomRepository.GetAllRooms())
            {
                if (r.Id == room.Id)
                { 
                    added = true;
                    break;
                }
            }

            if (!added)
                Assert.Fail();
        }

        [TestMethod]
        public void TestModifyRooms()
        {
            roomRepository.AddNewRooms(room);

            room.Floor = 12;
            roomRepository.ModifyRooms(room);
            foreach (Room x in roomRepository.GetAllRooms())
            {
                if (x.Id == room.Id)
                    if (room.Floor != x.Floor)
                        Assert.Fail();
            }
        }
        [TestMethod]
        public void TestDeleteRooms()
        {
            roomRepository.AddNewRooms(room);
            roomRepository.DeleteRooms(room.Id);

            foreach (Room x in roomRepository.GetAllRooms())
            {
                if (x.Id == room.Id)
                    Assert.Fail();
            }
        }

      
        [TestCleanup]
        public void Cleanup()
        {
            roomRepository.DeleteRooms(room.Id);
        }
    }
}
