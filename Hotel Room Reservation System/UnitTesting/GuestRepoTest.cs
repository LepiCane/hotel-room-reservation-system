﻿using System;
using System.Collections.Generic;
using DataLayer;
using DataLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTesting
{
    [TestClass]
    public class GuestRepoTest
    {
        private GuestRepository guestRepository = new GuestRepository();
        private Guest guest;

        [TestInitialize]
        public void init()
        {
            guest = new Guest();
            guest.Id = "TEST123ID1";
            guest.Firstname = "TESTFIRSTNAME";
            guest.Lastname = "TESTLASTNAME";
            guest.Address = "TESTADDRESS";
            guest.BirthDate = DateTime.Now;
            guest.Phone = "00000000";
            guest.Email = "testmail";
        }

        [TestMethod]
        public void TestAddNewGuest()
        {
            guestRepository.AddNewGuest(guest);

            bool added = false;
            foreach (Guest r in guestRepository.GetAllGuests())
            {
                if (r.Id == guest.Id)
                {
                    added = true;
                    break;
                }
            }

            if (!added)
                Assert.Fail();
        }
        [TestMethod]
        public void TestGetGuestById()
        {
            guestRepository.AddNewGuest(guest);

            Guest g = guestRepository.GetGuestById(guest.Id);

            if(g.Id != guest.Id || g == null)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TestDeleteGuestById()
        {
            guestRepository.AddNewGuest(guest);
            guestRepository.DeleteGuest(guest.Id);

            foreach (Guest x in guestRepository.GetAllGuests())
            {
                if (x.Id == guest.Id)
                   Assert.Fail();
            }
        }

        [TestCleanup]
        public void Cleanup()
        {
            guestRepository.DeleteGuest(guest.Id);
        }
    }
}
