﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class GuestBusiness
    {
        private GuestRepository guestRepository;

        public GuestBusiness()
        {
            this.guestRepository = new GuestRepository();
        }

        public List<Guest> GetAllGuests()
        {
            return guestRepository.GetAllGuests();
        }
        
        public bool AddNewGuest(Guest guest)
        {
            return guestRepository.AddNewGuest(guest);
        }

        public bool DeleteGuest(string id)
        {
            return guestRepository.DeleteGuest(id);
        }

        public Guest GetGuestById(string id)
        {
            return guestRepository.GetGuestById(id);
        }
    }
}
