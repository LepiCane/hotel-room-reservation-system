﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class RoomTypeBusiness
    {
        private RoomTypeRepository roomtypeRepository = new RoomTypeRepository();
        public List<RoomType> GetAllRoomTypes()
        {
            return roomtypeRepository.GetAllRoomTypes();
        }
        public bool AddNewRoomType(RoomType roomtype)
        {
            if (roomtypeRepository.AddNewRoomType(roomtype) > 0)
                return true;
            return false;
        }
        public RoomType GetRoomTypeByName(string name)
        {
            return roomtypeRepository.GetRoomTypeByName(name);
        }

        public bool RemoveRoomType(string name)
        {
            RoomType rt = new RoomType();
            rt.Type = name;
            if (roomtypeRepository.RemoveRoomType(rt) > 0)
                return true;
            return false;
        }
    }
}
