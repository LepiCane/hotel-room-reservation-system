﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class RoomBusiness
    {   

        private RoomRepository roomRepository = new RoomRepository();

        public List<Room> GetAllRooms()
        {
            return roomRepository.GetAllRooms();
        }

       
        public bool AddNewRooms(Room room)
        {
            return roomRepository.AddNewRooms(room);
        }

        public bool ModifyRooms(Room room)
        {
            return roomRepository.ModifyRooms(room);
        }

        public Room GetRoomById(int id)
        {
            return roomRepository.GetRoomById(id);
        }

        public bool DeleteRooms(int id)
        {
            return roomRepository.DeleteRooms(id);
             
        }
    }
}
