﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class ReservationBusiness
    {
        private ReservationRepository reservationRepository = new ReservationRepository();
        private RoomRepository roomRepository = new RoomRepository();

        public bool AddNewReservation(Reservation reservation)
        {
            return reservationRepository.AddNewReservation(reservation);
        }
        public List<Reservation> FreeRooms()
        {
            List<Reservation> reservations = new List<Reservation>();
            DateTime thisDay = DateTime.Today;
            foreach (Reservation r in reservationRepository.GetAllReservations())
            {
                if (r.EndDate < thisDay)
                    reservations.Add(r);
            }
            return reservations;
        }
        public List<Room> GetReservedRooms()
        {
            List<Reservation> reservations = reservationRepository.GetAllReservations();
            List<Room> rooms = roomRepository.GetAllRooms();
            List<Room> resRooms = new List<Room>();

            foreach (Room rum in rooms)
            {
                foreach (Reservation r in reservations)
                {
                
                    if (rum.Id == r.RoomId && r.EndDate > DateTime.Today)
                   
                        resRooms.Add(rum);
                }
            }
            return resRooms;
        }

        public List<Room> GetFreeRoom()
        {
            List<Room> freeRooms = new List<Room>();
            List<Room> rooms = roomRepository.GetAllRooms();

            List<Room> reservations = GetReservedRooms();
            foreach (Room rum in rooms)
            {
                bool res = false;
                foreach (Room r in reservations)
                {

                    if (rum.Id == r.Id )
                    {
                        res = true; break;
                    }
                        
                }
                if(!res)
                {
                    freeRooms.Add(rum);

                }
            }
            return freeRooms;

        }
        public Reservation GetReservationByGuest(Guest guest)
        {
            return reservationRepository.GetReservationByGuest(guest);
        }

        public bool DeleteReservation(int id)
        {
            return reservationRepository.DeleteReservation(id);
        }
    }
}
