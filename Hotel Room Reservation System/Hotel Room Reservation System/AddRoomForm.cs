﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DataLayer.Models;
using BusinessLayer;

namespace Hotel_Room_Reservation_System
{
    public partial class AddRoomForm : MetroFramework.Forms.MetroForm
    {
        private RoomBusiness roomBusiness;
        private RoomTypeBusiness roomTypeBusiness;

        public AddRoomForm()
        {
            InitializeComponent();
            this.roomBusiness = new RoomBusiness();
            this.roomTypeBusiness = new RoomTypeBusiness();
            metroTile1.StyleManager = metroStyleManager1;
            metroTile2.StyleManager = metroStyleManager2;
            metroTile3.StyleManager = metroStyleManager3;
        }

    private void refresh()
        {
            DataTable dt = (DataTable)dataGridView1.DataSource;
            dt.Clear();
            foreach (Room room in roomBusiness.GetAllRooms())
            {
                dt.Rows.Add(room.Id, room.TypeID, room.Category, room.Floor, room.Price, room.Note);
            }

            dataGridView1.DataSource = dt;
        }

        private void AddRoomForm_Load(object sender, EventArgs e)
        {
            // Get room types for combo box
            List<RoomType> roomTypes = roomTypeBusiness.GetAllRoomTypes();
            foreach (RoomType roomType in roomTypes)
            {
                comboBox1.Items.Add(roomType.Type);
            }

            for (int i = 1; i <= DashboardForm.hotel.numberOfFloors; i++)
            {
                comboBox2.Items.Add(i);
            }

            for (int i = 1; i <= DashboardForm.hotel.numberOfCategories; i++)
            {
                comboBox3.Items.Add(i);
            }
            DataTable dt = new DataTable();
            dt.Columns.Add("Id", typeof(int));
            dt.Columns.Add("TypeID", typeof(int));
            dt.Columns.Add("Category", typeof(int));
            dt.Columns.Add("Floor", typeof(int));
            dt.Columns.Add("Price", typeof(double));
            dt.Columns.Add("Note", typeof(string));

            foreach (Room room in roomBusiness.GetAllRooms())
            {
                dt.Rows.Add(room.Id, room.TypeID, room.Category, room.Floor, room.Price, room.Note);
            }

            dataGridView1.DataSource = dt;

        }



        private void metroTile1_Click(object sender, EventArgs e)
        {
            Room room = new Room();

            try
            {
                room.Id = Convert.ToInt32(textBox1.Text);
            }
            catch
            {
                MessageBox.Show("Must insert room Id!", "Invalid");
                return;
            }

            try
            {
                RoomType rt = roomTypeBusiness.GetRoomTypeByName(comboBox1.Text);
                room.TypeID = rt.Id;
            }
            catch
            {
                MessageBox.Show("Must insert Type!", "Invalid");
                return;
            }

            try
            {
                room.Floor = Convert.ToInt32(comboBox2.Text);
            }
            catch
            {
                MessageBox.Show("Must insert floor!", "Invalid");
                return;
            }

            try
            {
                room.Category = Convert.ToInt32(comboBox3.Text);
            }
            catch
            {
                MessageBox.Show("Must insert category!", "Invalid");
                return;
            }

            try
            {
                room.Price = Convert.ToDouble(textBox2.Text);
            }
            catch
            {
                MessageBox.Show("Must insert room Price!", "Invalid");
                return;
            }

            room.Note = textBox3.Text;

            if (roomBusiness.AddNewRooms(room))
            {
                MessageBox.Show("Added");
                refresh();
            }
            else
            {
                MessageBox.Show("Failed adding room!");
            }

        }

        private void metroTile2_Click(object sender, EventArgs e)
        {
            int rowindex = dataGridView1.CurrentRow.Index;

            int id = Convert.ToInt32(dataGridView1.Rows[rowindex].Cells[0].Value.ToString());

            if (id > 0)
            {
                roomBusiness.DeleteRooms(id);
                MessageBox.Show("Room delted");
                refresh();
                return;
            }

            MessageBox.Show("ERR");
        }

        private void metroTile3_Click(object sender, EventArgs e)
        {
            Room room = new Room();

            try
            {
                room.Id = Convert.ToInt32(textBox1.Text);
            }
            catch
            {
                MessageBox.Show("Must insert room Id!", "Invalid");
                return;
            }

            try
            {
                room.TypeID = comboBox1.SelectedIndex + 1;
            }
            catch
            {
                MessageBox.Show("Must insert Type!", "Invalid");
                return;
            }

            try
            {
                room.Floor = Convert.ToInt32(comboBox2.Text);
            }
            catch
            {
                MessageBox.Show("Must insert floor!", "Invalid");
                return;
            }

            try
            {
                room.Category = Convert.ToInt32(comboBox3.Text);
            }
            catch
            {
                MessageBox.Show("Must insert category!", "Invalid");
                return;
            }

            try
            {
                room.Price = Convert.ToDouble(textBox2.Text);
            }
            catch
            {
                MessageBox.Show("Must insert room Price!", "Invalid");
                return;
            }

            room.Note = textBox3.Text;
            if (roomBusiness.ModifyRooms(room))
            {
                MessageBox.Show("DONE");
                refresh();
                return;
            }
            MessageBox.Show("Er");
        }
    }
}
