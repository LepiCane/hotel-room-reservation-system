﻿using BusinessLayer;
using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hotel_Room_Reservation_System
{
    public partial class CheckoutGuestForm : MetroFramework.Forms.MetroForm
    {
        private GuestBusiness guestBusiness;
        private ReservationBusiness reservationBusiness;
        private RoomBusiness roomBusiness;

        private Guest guest;
        private Reservation reservation;

        public CheckoutGuestForm()
        {
            InitializeComponent();
            this.guestBusiness = new GuestBusiness();
            this.reservationBusiness = new ReservationBusiness();
            this.roomBusiness = new RoomBusiness();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string id = textBox1.Text;
            guest = guestBusiness.GetGuestById(id);

            if(guest == null)
            {
                MessageBox.Show("Guest not found!");
                return;
            }

            textBox2.Text = guest.Firstname;
            textBox5.Text = guest.Lastname;
            textBox8.Text = guest.Address;
            textBox3.Text = guest.Email;
            textBox6.Text = guest.Phone;

            reservation = reservationBusiness.GetReservationByGuest(guest);

            if(reservation == null)
            {
                MessageBox.Show("Cannot find reservation");
                return;
            }

            Room room = roomBusiness.GetRoomById(reservation.RoomId);
            textBox4.Text = reservation.RoomId.ToString();
            textBox7.Text = room.Category.ToString();

            label12.Text = Convert.ToString( room.Price * ((reservation.EndDate - reservation.BeginDate).TotalDays + 1)) + "$";

        }

    

        private void CheckoutGuestForm_Load(object sender, EventArgs e)
        {
            textBox1.Text = textBox2.Text = textBox3.Text = textBox4.Text =
            textBox5.Text = textBox6.Text = textBox7.Text = textBox8.Text = "";
        }

        private void metroTile2_Click(object sender, EventArgs e)
        {
            try
            { 
            if (guest != null)
            {
                if (reservationBusiness.DeleteReservation(reservation.Id))
                {
                    MessageBox.Show("Deleted reservation");
                }
                if (guestBusiness.DeleteGuest(guest.Id))
                {
                    MessageBox.Show("User checkoed out!");
                }

            }
            }
            catch
            {

            }
        }
    }
}
