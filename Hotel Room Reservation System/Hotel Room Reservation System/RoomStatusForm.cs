﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hotel_Room_Reservation_System
{
    public partial class RoomStatusForm : MetroFramework.Forms.MetroForm
    {
        private RoomBusiness roomBusiness;
        private ReservationBusiness reservationBusiness;
        private RoomTypeBusiness roomTypeBusiness;

        public RoomStatusForm()
        {
            InitializeComponent();

            this.roomBusiness = new RoomBusiness();
            this.reservationBusiness = new ReservationBusiness();
            this.roomTypeBusiness = new RoomTypeBusiness();
            metroTile2.StyleManager = metroStyleManager2; 
        }

        private void RoomStatusForm_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Number",    typeof(int));
            dt.Columns.Add("Type",      typeof(string));
            dt.Columns.Add("Category",  typeof(int));
            dt.Columns.Add("Floor",     typeof(int));
            dt.Columns.Add("Price",     typeof(double));
            dt.Columns.Add("Note",      typeof(string));
            dataGridView1.DataSource = dt;
        }

   

        private void metroTile1_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dataGridView1.DataSource;
            dt.Clear();

            if (checkBox1.Checked && checkBox2.Checked)
            {
                List<Room> rooms = roomBusiness.GetAllRooms();
                foreach (Room room in rooms)
                {
                    string type = "";
                    List<RoomType> roomType = roomTypeBusiness.GetAllRoomTypes();
                    foreach (RoomType r in roomType)
                    {
                        if (room.TypeID == r.Id)
                        {
                            type = r.Type;
                            break;
                        }
                    }
                    dt.Rows.Add(room.Id, type, room.Category, room.Floor, room.Price, room.Note);
                }
            }
            else if (checkBox1.Checked == false && checkBox2.Checked)
            {
                List<Room> rooms = reservationBusiness.GetReservedRooms();

                foreach (Room room in rooms)
                {
                    string type = "";
                    List<RoomType> roomType = roomTypeBusiness.GetAllRoomTypes();
                    foreach (RoomType r in roomType)
                    {
                        if (room.TypeID == r.Id)
                        {
                            type = r.Type;
                            break;
                        }
                    }
                    dt.Rows.Add(room.Id, type, room.Category, room.Floor, room.Price, room.Note);
                }
            }
            else if (checkBox1.Checked && checkBox2.Checked == false)
            {
                List<Room> rooms = reservationBusiness.GetFreeRoom();
                foreach (Room room in rooms)
                {
                    string type = "";
                    List<RoomType> roomType = roomTypeBusiness.GetAllRoomTypes();
                    foreach (RoomType r in roomType)
                    {
                        if (room.TypeID == r.Id)
                        {
                            type = r.Type;
                            break;
                        }
                    }
                    dt.Rows.Add(room.Id, type, room.Category, room.Floor, room.Price, room.Note);
                }
            }
            dataGridView1.DataSource = dt;
        }

        private void metroTile2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
