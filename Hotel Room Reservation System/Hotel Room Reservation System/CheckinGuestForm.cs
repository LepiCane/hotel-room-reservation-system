﻿using DataLayer.Models;
using DataLayer;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BusinessLayer;

namespace Hotel_Room_Reservation_System
{
    public partial class CheckinGuestForm : MetroFramework.Forms.MetroForm
    {
        private GuestBusiness guestBusiness;
        private ReservationBusiness reservationBusiness;

        public CheckinGuestForm()
        {
            InitializeComponent();

            this.guestBusiness = new GuestBusiness();
            this.reservationBusiness = new ReservationBusiness();
            this.MaximizeBox = false;
            this.metroTile1.StyleManager = metroStyleManager1;
            this.metroTile2.StyleManager = metroStyleManager2;
        }

   

        private void metroTile1_Click(object sender, EventArgs e)
        {
            Guest guest = new Guest();


            guest.Id = textBox1.Text;           //  Get guest id
            if (string.IsNullOrEmpty(guest.Id))
            {
                MessageBox.Show("Must insert guest Id!", "Invalid");
                return;
            }

            guest.Firstname = textBox2.Text;    //  Get guest firstname
            if (string.IsNullOrEmpty(guest.Firstname))
            {
                MessageBox.Show("Must insert guest Firstname!", "Invalid");
                return;
            }

            guest.Lastname = textBox3.Text;     //  Get guest lastname
            if (string.IsNullOrEmpty(guest.Lastname))
            {
                MessageBox.Show("Must insert guest Lastname!", "Invalid");
                return;
            }

            guest.Address = textBox4.Text;      //  Get guest address
            if (string.IsNullOrEmpty(guest.Address))
            {
                MessageBox.Show("Must insert guest Address!", "Invalid");
                return;
            }

            guest.Email = textBox5.Text;        // Get guest email
            if (string.IsNullOrEmpty(guest.Email))
            {
                MessageBox.Show("Must insert guest Email!", "Invalid");
                return;
            }

            guest.Phone = textBox6.Text;        // Get guest phone
            if (string.IsNullOrEmpty(guest.Phone))
            {
                MessageBox.Show("Must insert guest Phone!", "Invalid");
                return;
            }

            guest.BirthDate = dateTimePicker1.Value;    // Get guest birth date

            uint roomNumber;

            try
            {
                roomNumber = Convert.ToUInt32(textBox7.Text);
            }
            catch
            {
                MessageBox.Show("Must be valid room number", "Invalid");
                return;
            }


            // Now everything should be valid and redy for inserting to db

            if (!guestBusiness.AddNewGuest(guest))
            {
                MessageBox.Show("FAILED ADDING NEW GUEST!");
                return;
            }

            Reservation reservation = new Reservation();
            reservation.GuestId = guest.Id;

            reservation.RoomId = (int)roomNumber;
            reservation.Payment = 0;
            reservation.BeginDate = dateTimePicker2.Value;
            reservation.EndDate = dateTimePicker3.Value;

            if (!reservationBusiness.AddNewReservation(reservation))
            {
                MessageBox.Show("FAILED CREATING RESERVATION");
                guestBusiness.DeleteGuest(guest.Id);
                return;
            }
            else
            {
                MessageBox.Show("Done: User checked in!");
                textBox1.Text = textBox2.Text = textBox3.Text = textBox4.Text = textBox5.Text = textBox6.Text = textBox7.Text = "";
            }
        }

        private void metroTile2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
