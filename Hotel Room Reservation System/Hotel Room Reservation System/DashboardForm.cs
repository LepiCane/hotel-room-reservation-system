﻿using DataLayer.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hotel_Room_Reservation_System
{
    public partial class DashboardForm : MetroFramework.Forms.MetroForm
    {
        private CheckinGuestForm checkinGuestForm;
        private CheckoutGuestForm checkoutGuestForm;
        private AddRoomForm addRoomForm;
        private RoomStatusForm roomStatusForm;

        public static Hotel hotel;
        private SettingsForm settingsForm;

        public DashboardForm()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.timer1.Start();
        }

     
      
      

        private void DashboardForm_Load(object sender, EventArgs e)
        {
            checkinGuestForm = new CheckinGuestForm();
            checkoutGuestForm = new CheckoutGuestForm();
            addRoomForm = new AddRoomForm();
            roomStatusForm = new RoomStatusForm();
            hotel = new Hotel();

            try
            { 
                string settingsData = File.ReadAllText("settings.json");
                hotel = JsonConvert.DeserializeObject<Hotel>(settingsData);
            }
            catch
            {
                hotel.name = "unnamed";
                hotel.numberOfFloors = 1;
                hotel.numberOfCategories = 1;
                using (StreamWriter sw = new StreamWriter("settings.json"))
                {
                    sw.WriteLine(JsonConvert.SerializeObject(hotel));
                }
            }
            Text = "Dashboard : " + hotel.name;
        }


        private void metroTile1_Click(object sender, EventArgs e)
        {
            try
            {
                checkinGuestForm.Show();
            }
            catch
            {
                checkinGuestForm = new CheckinGuestForm();
                checkinGuestForm.Show();
            }
        }

        private void metroTile2_Click(object sender, EventArgs e)
        {
            try
            {
                roomStatusForm.Show();
            }
            catch
            {
                roomStatusForm = new RoomStatusForm();
                roomStatusForm.Show();
            }
        }

        private void metroTile3_Click(object sender, EventArgs e)
        {
            try
            {
                checkoutGuestForm.Show();
            }
            catch
            {
                checkoutGuestForm = new CheckoutGuestForm();
                checkoutGuestForm.Show();
            }
        }

        private void metroTile4_Click(object sender, EventArgs e)
        {
            try
            {
                addRoomForm.Show();
            }
            catch
            {
                addRoomForm = new AddRoomForm();
                addRoomForm.Show();
            }
        }

        private void metroTile5_Click(object sender, EventArgs e)
        {
            try
            {
                settingsForm.Show();
            }
            catch
            {
                settingsForm = new SettingsForm(this, hotel);
                settingsForm.Show();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            metroLabel1.Text = DateTime.Now.ToShortTimeString();
        }
    }
}
