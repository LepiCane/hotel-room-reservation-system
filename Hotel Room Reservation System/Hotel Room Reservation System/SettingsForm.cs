﻿using BusinessLayer;
using DataLayer.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hotel_Room_Reservation_System
{
    public partial class SettingsForm : MetroFramework.Forms.MetroForm
    {
        private Hotel hotel;
        private DashboardForm df;
        private RoomTypeBusiness roomTypeBusiness;

        public SettingsForm(DashboardForm df, Hotel hotel)
        {
            InitializeComponent();
            this.hotel = hotel;
            this.roomTypeBusiness = new RoomTypeBusiness();
            this.df = df;
            this.MaximizeBox = false;
        }



        private void button3_Click(object sender, EventArgs e)
        {
            string roomType = textBox4.Text;
            RoomType rt = new RoomType();
            rt.Type = roomType;
            roomTypeBusiness.AddNewRoomType(rt);
            listBox1.Items.Clear();
            List<RoomType> roomTypes = roomTypeBusiness.GetAllRoomTypes();
            foreach (RoomType rti in roomTypes)
            {
                listBox1.Items.Add(rti.Type);
            }
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            List<RoomType> roomTypes = roomTypeBusiness.GetAllRoomTypes();
            foreach(RoomType rt in roomTypes)
            {
                listBox1.Items.Add(rt.Type);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            { 
                string name = listBox1.SelectedItem.ToString();
                roomTypeBusiness.RemoveRoomType(name);
                listBox1.Items.Clear();
                List<RoomType> roomTypes = roomTypeBusiness.GetAllRoomTypes();
                foreach (RoomType rt in roomTypes)
                {
                    listBox1.Items.Add(rt.Type);
                }
            }
            catch
            {

            }
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            hotel.name = textBox1.Text;
            try
            {
                hotel.numberOfFloors = Convert.ToUInt32(textBox2.Text);
            }
            catch
            {
                MessageBox.Show("Invalid number of floors!");
                return;
            }

            try
            {
                hotel.numberOfCategories = Convert.ToUInt32(textBox3.Text);
            }
            catch
            {
                MessageBox.Show("Invalid number of categories!");
                return;
            }

            string json = JsonConvert.SerializeObject(hotel);
            using (StreamWriter writer = new StreamWriter("settings.json"))
            {
                writer.Write(json);
                MessageBox.Show("Successs");
                df.Text = "Dashboard : " + textBox1.Text;
                this.Hide();
            }
        }

        private void metroTile2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
