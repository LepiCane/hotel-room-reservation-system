﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class RoomRepository
    {
        private string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=\"Hotel Room Reservation DB\";Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public List<Room> GetAllRooms()
        {
            List<Room> rooms = new List<Room>();

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM Rooms";

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                while(sqlDataReader.Read())
                {
                    Room room = new Room();
                    room.Id = sqlDataReader.GetInt32(0);
                    room.TypeID = sqlDataReader.GetInt32(1);
                    room.Category = sqlDataReader.GetInt32(2);
                    room.Floor = sqlDataReader.GetInt32(3);
                    room.Price = sqlDataReader.GetDouble(4);
                    room.Note = sqlDataReader.GetString(5);

                    rooms.Add(room);
                }
                sqlConnection.Close();
            }
            return rooms;
        }

        public bool AddNewRooms(Room room)
        {
            int result;
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = string.Format("INSERT INTO Rooms VALUES('{0}','{1}','{2}','{3}','{4}','{5}')", room.Id, room.TypeID, room.Category, room.Floor, room.Price,  room.Note);

                try
                { 
                    result = sqlCommand.ExecuteNonQuery();
                }
                catch
                {
                    result = 0;
                }
            }
            return Convert.ToBoolean(result);
        }

       public bool ModifyRooms(Room room)
        {
            int res;
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = string.Format("UPDATE Rooms SET Id='{0}', TypeId='{1}', Category='{2}', Floor='{3}', Price='{4}', Note='{5}' WHERE Id={0}",room.Id, room.TypeID, room.Category, room.Floor, room.Price, room.Note);

                try
                {
                    res = sqlCommand.ExecuteNonQuery();
                }
                catch
                {
                    res = 0;
                }
            }
            return Convert.ToBoolean(res);
        }

        public bool DeleteRooms(int id )
        {
            int result;
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = string.Format("DELETE FROM Rooms WHERE Id={0}", id);

                try
                {
                    result = sqlCommand.ExecuteNonQuery();
                }
                catch
                {
                    result = 0;
                }
            }
            return Convert.ToBoolean(result);
        }

        public Room GetRoomById(int id)
        {
            

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM Rooms WHERE Id=" + id.ToString(); 

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    Room room = new Room();
                    room.Id = sqlDataReader.GetInt32(0);
                    room.TypeID = sqlDataReader.GetInt32(1);
                    room.Category = sqlDataReader.GetInt32(2);
                    room.Floor = sqlDataReader.GetInt32(3);
                    room.Price = sqlDataReader.GetDouble(4);
                    room.Note = sqlDataReader.GetString(5);

                    return room;
                }
                return null;
            }
        }
    }
}
