﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class GuestRepository
    {
        private string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=\"Hotel Room Reservation DB\";Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public List<Guest> GetAllGuests()
        {
            List<Guest> guests = new List<Guest>();

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM Guests";

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    Guest guest = new Guest();
                    guest.Id = sqlDataReader.GetString(0);
                    guest.Firstname = sqlDataReader.GetString(1);
                    guest.Lastname = sqlDataReader.GetString(2);
                    guest.Address = sqlDataReader.GetString(3);
                    guest.Email = sqlDataReader.GetString(4);
                    guest.Phone = sqlDataReader.GetString(5);
                    guest.BirthDate = sqlDataReader.GetDateTime(6);

                    guests.Add(guest);
                }

                sqlConnection.Close();
            }

            return guests;
        }

        public bool AddNewGuest(Guest guest)
        {
            int result;
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = string.Format("INSERT INTO Guests VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}')", guest.Id, guest.Firstname, guest.Lastname, guest.Address, guest.Email, guest.Phone, guest.BirthDate.ToShortDateString());

                try
                {
                    result = sqlCommand.ExecuteNonQuery();
                }
                catch
                {
                    result = 0;
                }
            }
            
            return Convert.ToBoolean(result);
        }

        public bool DeleteGuest(string id)
        {

            int result;
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = string.Format("DELETE FROM Guests WHERE Id='{0}'",id);

                try
                {
                    result = sqlCommand.ExecuteNonQuery();
                }
                catch
                {
                    result = 0;
                }
            }
            return Convert.ToBoolean(result);
        }


        public Guest GetGuestById(string id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = string.Format("SELECT * FROM Guests WHERE Id='{0}'", id);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    Guest guest = new Guest();
                    guest.Id = sqlDataReader.GetString(0);
                    guest.Firstname = sqlDataReader.GetString(1);
                    guest.Lastname = sqlDataReader.GetString(2);
                    guest.Address = sqlDataReader.GetString(3);
                    guest.Email = sqlDataReader.GetString(4);
                    guest.Phone = sqlDataReader.GetString(5);
                    guest.BirthDate = sqlDataReader.GetDateTime(6);

                    return guest;
                }
                return null;
            }
        }
    }
}
