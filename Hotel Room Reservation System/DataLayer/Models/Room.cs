﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
     public class Room
    {
        public int Id { get; set; }
        public int TypeID { get; set; }
        public int Category { get; set; }
        public int Floor { get; set; }
        public double Price { get; set; }
        public string Note { get; set; }
    }
}
