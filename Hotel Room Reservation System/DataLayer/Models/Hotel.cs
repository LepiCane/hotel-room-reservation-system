﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Hotel
    {
        public string name;
        public uint numberOfFloors;
        public uint numberOfCategories;
    }
}
