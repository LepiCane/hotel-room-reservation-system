﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class ReservationRepository
    {
        private string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=\"Hotel Room Reservation DB\";Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        public List<Reservation> GetAllReservations()
        {
            List<Reservation> reservations=new List<Reservation>();
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM Reservations";

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                while(sqlDataReader.Read())
                {
                    Reservation reservation = new Reservation();
                    reservation.Id = sqlDataReader.GetInt32(0);
                    reservation.GuestId = sqlDataReader.GetString(1);
                    reservation.RoomId = sqlDataReader.GetInt32(2);
                    reservation.Payment = sqlDataReader.GetDouble(3);
                    reservation.BeginDate = sqlDataReader.GetDateTime(4);
                    reservation.EndDate = sqlDataReader.GetDateTime(5);

                    reservations.Add(reservation);
                }
                return reservations;
            }
        }

        public bool AddNewReservation(Reservation reservation)
        {
            int result;
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = string.Format("INSERT INTO Reservations VALUES('{0}','{1}','{2}','{3}','{4}')", reservation.GuestId, reservation.RoomId, reservation.Payment.ToString(), reservation.BeginDate.ToShortDateString(), reservation.EndDate.ToShortDateString());

                try
                {
                    result = sqlCommand.ExecuteNonQuery();
                }
                catch
                {
                    result = 0;
                }
            }

            return Convert.ToBoolean(result);
        }

        public Reservation GetReservationByGuest(Guest guest)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = string.Format("SELECT * FROM Reservations WHERE GuestId='{0}'", guest.Id);

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    Reservation reservation = new Reservation();
                    reservation.Id = sqlDataReader.GetInt32(0);
                    reservation.GuestId = sqlDataReader.GetString(1);
                    reservation.RoomId = sqlDataReader.GetInt32(2);
                    reservation.Payment = sqlDataReader.GetDouble(3);
                    reservation.BeginDate = sqlDataReader.GetDateTime(4);
                    reservation.EndDate = sqlDataReader.GetDateTime(5);

                    return reservation;
                }
                return null;
            }
        }
        public bool DeleteReservation(int id)
        {

            int result;
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = string.Format("DELETE FROM Reservations WHERE Id={0}", id);

                try
                {
                    result = sqlCommand.ExecuteNonQuery();
                }
                catch
                {
                    result = 0;
                }
            }
            return Convert.ToBoolean(result);
        }
    }
}
