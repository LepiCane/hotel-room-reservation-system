﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    
    public class RoomTypeRepository
    {
        private string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=\"Hotel Room Reservation DB\";Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";



        public List<RoomType> GetAllRoomTypes()
        {
            List < RoomType > roomtypes = new List<RoomType>();

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM RoomTypes";

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    RoomType roomtype = new  RoomType();
                    roomtype.Id = sqlDataReader.GetInt32(0);
                    roomtype.Type = sqlDataReader.GetString(1);
                    roomtypes.Add(roomtype);
                }

                sqlConnection.Close();
            }

            return roomtypes;
        }

        public int AddNewRoomType(RoomType roomType)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = string.Format("INSERT INTO RoomTypes VALUES('{0}')", roomType.Type);


                return sqlCommand.ExecuteNonQuery();
            }
        }
        public int RemoveRoomType(RoomType roomType)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = string.Format("DELETE FROM RoomTypes WHERE Type='{0}'", roomType.Type);

                try
                {
                    return sqlCommand.ExecuteNonQuery();
                }
                catch
                {
                    return 0;
                }
            }
        }
        public RoomType GetRoomTypeByName(string name)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM RoomTypes";

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    RoomType roomtype = new RoomType();
                    roomtype.Id = sqlDataReader.GetInt32(0);
                    roomtype.Type = sqlDataReader.GetString(1);
                    if (roomtype.Type == name)
                        return roomtype;
                }

                return null;
            }
        }
    }
}
